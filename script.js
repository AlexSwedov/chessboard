/**
 * Завдання 4.
 * Написати функцію fillChessBoard, яка розгорне «шахівницю» 8 на 8.
 * Колір темних осередків - #161619.
 * Колір світлих осередків - #FFFFFF.
 * Інші стилі CSS для дошки та осередків готові.
 * Дошку необхідно розгорнути всередині елемента з класом .board.
 * Кожна комірка дошки представляє елемент div із класом .cell.
 * <div class="board">
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      <div class="cell"></div>
      ...
   </div>
 */

const fillChessBoard = () => {
	const LIGHT_CELL = '#ffffff';
	const DARK_CELL = '#161619';
	const V_CELLS = 8;
	const H_CELLS = 8;
	const board = document.createElement('div');
	board.classList.add('board');
	document.body.append(board);
	const multipliCell = V_CELLS * H_CELLS;

	for (let i = 0; i < multipliCell; i++) {
		const cell = document.createElement('div');
		cell.classList.add('cell');
		board.append(cell);

		if ((i % 16 < 8 && i % 2 === 0) || (i % 16 >= 8 && i % 2 === 1)) {
			cell.style.backgroundColor = DARK_CELL;
		} else {
			cell.style.backgroundColor = LIGHT_CELL;
		}
	}
}

fillChessBoard()
